## Postman Exports
This repository contains files which you can import into your postman to set up a collection (set of requests) and environment (global variables). You just hit the import button at the top of the page 
in Postman and select 'import'.

Please see here for a guide for using postman collections and environments: https://www.evernote.com/shard/s210/nl/26196564/ae01289a-d8e7-4720-9e96-4c9020e34176